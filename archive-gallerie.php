<?php
get_header();
?>

<div class="container mt-4">
    <div class="jumbotron">
        <h1 class="text-center">Gallerie<h1>
    </div>
    
    <div class="row">
    
<?php
if(have_posts()){
    while(have_posts()){
        the_post();
?>
                <div class="mb-4 col-md-4">
                    <div class="card">
                        <div class="col-md-3 col-6 mb-4"><?php the_post_thumbnail(); ?> </div>
                    </div>
                </div>
            <?php
    }
}
?>

<?php
get_footer();
?>


