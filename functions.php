<?php

// Ajouter des images mises en avant
add_theme_support( 'post-thumbnails' );


if(!function_exists('group')){
    function group(){

   /**
    * Custom Post Type : Membres
    */

    $labels1 = array(
        "name"                  => __( "Membres", "enssop" ),
        "singular_name"         => __( "Membres", "enssop" ),
        "menu_name"             => __( "Membres", "enssop" ),
        "all_items"             => __( "Tous les membres", "enssop" ),
        "add_new"               => __( "Ajouter un membre", "enssop" ),
        "add_new_item"          => __( "Ajouter un nouveau membre", "enssop" ),
        "edit_item"             => __( "Ajouter un membre", "enssop" ),
        "new_item"              => __( "Nouveau membre", "enssop" ),
        "view_item"             => __( "Voir le membre", "enssop" ),
        "view_items"            => __( "Voir les membres", "enssop" ),
        "search_items"          => __( "Chercher un membre", "enssop" ),
        "not_found"             => __( "Pas de membres trouvé", "enssop" ),
        "not_found_in_trash"    => __( "Pas de membres trouvé dans la corbeille", "enssop" ),
        "featured_image"        => __( "Image mise en avant pour ce membre", "enssop" ),
        "set_featured_image"    => __( "Définir l'image mise en avant pour ce membre", "enssop" ),
        "remove_featured_image" => __( "Supprimer l'image mise en avant pour ce membre", "enssop" ),
        "use_featured_image"    => __( "Utiliser comme image mise en avant pour ce membre", "enssop" ),
        "archives"              => __( "Type de membre", "enssop" ),
        "insert_into_item"      => __( "Ajouter au membre", "enssop" ),
        "uploaded_to_this_item" => __( "Ajouter au membre", "enssop" ),
        "filter_items_list"     => __( "Filtrer la liste des membres", "enssop" ),
        "items_list_navigation" => __( "Naviguer dans la liste des membres", "enssop" ),
        "items_list"            => __( "Liste des membres", "enssop" ),
        "attributes"            => __( "Paramètres des membres", "enssop" ),
        "name_admin_bar"        => __( "Membres", "enssop" ),
        );
     
        $args1= array(
            "label"     => __('Membres', 'enssop'),
            "labels"    => $labels1,
            "description"   =>  __('Membres du groupe Deep Purple', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "membre", "with_front" => true ),
            "query_var"             => 'membre',
            "menu_icon"             => "dashicons-format-audio",
            "supports"              => array(  'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),

        );
     
         register_post_type( 'membre', $args1 );

            /**
    * Custom Post Type : Gallerie
    */

    $labels1 = array(
        "name"                  => __( "Gallerie", "enssop" ),
        "singular_name"         => __( "Gallerie", "enssop" ),
        "menu_name"             => __( "Gallerie", "enssop" ),
        "all_items"             => __( "Toutes les galleries", "enssop" ),
        "add_new"               => __( "Ajouter une gallerie", "enssop" ),
        "add_new_item"          => __( "Ajouter un nouvelle gallerie", "enssop" ),
        "edit_item"             => __( "Ajouter une gallerie", "enssop" ),
        "new_item"              => __( "Nouvelle gallerie", "enssop" ),
        "view_item"             => __( "Voir la gallerie", "enssop" ),
        "view_items"            => __( "Voir les galleries", "enssop" ),
        "search_items"          => __( "Chercher une gallerie", "enssop" ),
        "not_found"             => __( "Pas de gallerie trouvée", "enssop" ),
        "not_found_in_trash"    => __( "Pas de gallerie trouvée dans la corbeille", "enssop" ),
        "featured_image"        => __( "Image mise en avant pour cette gallerie", "enssop" ),
        "set_featured_image"    => __( "Définir l'image mise en avant pour cette gallerie", "enssop" ),
        "remove_featured_image" => __( "Supprimer l'image mise en avant pour cette gallerie", "enssop" ),
        "use_featured_image"    => __( "Utiliser comme image mise en avant pour cette gallerie", "enssop" ),
        "archives"              => __( "Type de gallerie", "enssop" ),
        "insert_into_item"      => __( "Ajouter à la gallerie", "enssop" ),
        "uploaded_to_this_item" => __( "Ajouter à la gallerie", "enssop" ),
        "filter_items_list"     => __( "Filtrer la liste des galleries", "enssop" ),
        "items_list_navigation" => __( "Naviguer dans la liste des galleries", "enssop" ),
        "items_list"            => __( "Liste des galleries", "enssop" ),
        "attributes"            => __( "Paramètres des galleries", "enssop" ),
        "name_admin_bar"        => __( "Gallerie", "enssop" ),
        );
     
        $args1= array(
            "label"     => __('Gallerie', 'enssop'),
            "labels"    => $labels1,
            "description"   =>  __('Gallerie du groupe Deep Purple', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "gallerie", "with_front" => true ),
            "query_var"             => 'gallerie',
            "menu_icon"             => "dashicons-money",
            "supports"              => array(  'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),

        );
     
         register_post_type( 'gallerie', $args1 );


    }
}
         
         add_action( 'init', 'group');    
