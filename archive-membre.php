<?php
get_header();
?>
<div class="container mt-4">
    <div class="jumbotron">
        <h1 class="text-center">Membres</h1>
    </div>

    <div class="row">
<?php
if(have_posts()){
    while(have_posts()){
        the_post();
?>
                <div class="mb-4 col-md-4">
                    <div class="card">
                        <?php the_post_thumbnail(); ?>
                        <div class="card-body">
                            <p class="card-text"><?php the_title(); ?></p>
                            <div class="btn-group">
                            <a href= "<?php the_permalink();?>"class='btn btn-sm btn-outline-success'>View</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
    }
}
?>
<?php
get_footer();
?>
