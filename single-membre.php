<?php get_header(); ?>

<?php
if(have_posts()){
    while(have_posts()){
        the_post();
        ?>
            <div class="container mt-4">
                <div class="jumbotron">
                    <h1 class="text-center"><?php the_title(); ?></h1>
                </div>
                <div class="row">
                    <div class="col-md-6" id="pictureMembre">
                    <img src="<?php the_post_thumbnail_url() ?>" class="w-100" alt="">
                    </div>
                    <div class="col-md-6"><?php the_content(); ?></div>
                </div>
        <?php
    }
}
?>

<?php get_footer(); ?>
